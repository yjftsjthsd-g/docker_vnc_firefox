FROM alpine:3.17
RUN apk add openbox xterm font-noto dbus firefox-esr shadow x11vnc xvfb
RUN ["useradd", "-m", "user"]
COPY ./start.sh /opt/start.sh
RUN chmod +x /opt/start.sh
USER user
CMD /opt/start.sh

