#!/bin/sh

VNC_GEOMETRY="${VNC_GEOMETRY:-1024x768x16}"

Xvfb -nolisten tcp -screen 0 "${VNC_GEOMETRY}" :1 &
x11vnc -forever -shared -display :1 &

sleep 1

export DISPLAY=:1
firefox &
openbox

